# Sparten Robber
A [game](https://smorty100.itch.io/sparten-robber) made for the Scream Jam 2023 on itch.io


You are free to use any assets in the project, as they are all from Creative Commons sources, specifically from polyheaven.com

**Important note!**
For opening the project, I strongly recommend to use Godot 4.2 beta 4, as this is the version I used when making this game. On 4.1.3 stable I saw some issues with weird vertex manipulation and generally meshes being corrupted.
