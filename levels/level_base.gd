extends Node3D
class_name LevelBase

@onready var itemElement:HBoxContainer = preload("res://scenes/list_item.tscn").instantiate()

var difficulty:int = 0

var list:Array



const objects:Array[String] = ["Apple", "Lime", "Pears", "PinkCleaner", "Pomegranate", "Whine"]


func set_lights(state:bool = true):
	if state == true:
		$World/Lights.visible = true
		$World/LightsOff.visible = false
		$World/LightsOn.visible = true
	if state == false:
		$World/Lights.visible = false
		$World/LightsOff.visible = true
		$World/LightsOn.visible = false



func _ready():
	if get_tree().get_first_node_in_group("follower"):
		follower = get_tree().get_first_node_in_group("follower").get_node("Follower")
	homeMenu = load("res://scenes/home_menu.tscn")
	if SaveSystem.get_var("is_hard") == true:
		difficulty = 1
	match difficulty:
		0:
			list = [3, 3, 3, 3, 3, 3]
		1:
			list = [6, 6, 6, 6, 6, 6]
			hide_flash_info()
			%ListInfo.visible = false
	%UI.get_node("NotCon/AnimationPlayer").play("RESET")
	add_items()


var notiIsWaiting:bool = false
func noti(msg:String):
	%UI.get_node("NotCon/Not/Label").text = msg
	%UI.get_node("NotCon/AnimationPlayer").play("open")
	if notiIsWaiting == false:
		notiIsWaiting = true
		await get_tree().create_timer(2.0).timeout
		%UI.get_node("NotCon/AnimationPlayer").play_backwards("open")
		await %UI.get_node("NotCon/AnimationPlayer").animation_finished
		notiIsWaiting = false


func retry():
	get_tree().reload_current_scene()

var homeMenu:PackedScene

func home_menu():
	get_tree().change_scene_to_packed(homeMenu)


var collectedOne:bool = false

func lose():
	if collectedOne == true:
		print("You lost")
		%LoseMenu.popup()
	else:
		print("You became friends")
		%GoodMenu.popup()

var isChasing:bool = false

func chase():
	follower.movementSpeed += 0.4
	%Lights.visible = false
	%Exit.get_node("Collision").disabled = false
	if isChasing == false:
		print("you are being chased")
		%ChaseMenu.popup()
	isEnd = true
	isChasing = true
	follower.isFollowing = true
	follower.get_parent().get_node("NavigationAgent3D").target_position = get_tree().get_first_node_in_group("player").global_position


func win():
	if difficulty == 0:
		print("you won")
		%WinMenu.popup()
	if difficulty == 1:
		print("you won the hard way")
		%HardMenu.popup()

var flashTime:float = 30.0

var isFlashing:bool = false:
	set(new):
		isFlashing = new
		if get_tree().get_first_node_in_group("follower"):
			get_tree().get_first_node_in_group("follower").get_node("Follower").canSee = new

func can_flash() -> bool:
	if flashTime > 0:
		return true
	else: return false


var isEnd:bool = false


var follower:Node

func exit_store():
	if isEnd == true:
		if difficulty == 0:
			%SuccessMenu.popup()
		if difficulty == 1:
			%HardMenu.popup()
	else:
		noti("There is no reason to leave the shop!")

func collect(type:String):
	collectedOne = true
	follower.canSee = true
	follower.isFollowing = true
	for i in objects.size():
		if objects[i] == type:
			%Collect.play()
			if list[i] - 1 >= 0:
				list[i] -= 1
				noti("Collected " + objects[i])
			else:
				noti("You already have\n" + objects[i])
			%UI.get_node("Control/ShoppingList/VBoxContainer").get_child(i + 1).get_node("Label").text = str(list[i]) + " " + objects[i]
	follower.movementSpeed += 0.1
	
	
	var overZero:bool = false
	for i in list:
		if i > 0:
			overZero = true
			break
	
	if overZero == false:
		chase()
	

func pause():
	get_tree().paused = true

func play():
	get_tree().paused = false

func add_items():
	for i in list.size():
		var duplicat:HBoxContainer = itemElement.duplicate()
		duplicat.get_node("Label").text = str(list[i]) + objects[i]
		duplicat.name = objects[i]
		%UI.get_node("Control/ShoppingList/VBoxContainer").add_child(duplicat)

var listOpen:bool = false


func hide_flash_info():
	%FlashInfo.visible = false

func _process(delta):
	if Input.is_action_just_pressed("ui_cancel"):
		%PauseMenu.popup()
	
	if isEnd == true:
		follower.get_parent().get_node("NavigationAgent3D").target_position = get_tree().get_first_node_in_group("player").global_position
	
	
	if Input.is_action_just_pressed("ui_cancel"):
		Input.mouse_mode = Input.MOUSE_MODE_VISIBLE
	if timer <= 0:
		stamina += delta * 2 * 30
	stamina = clamp(stamina, 0, 100)
	if Input.is_action_just_pressed("open_list"):
		%ListInfo.visible = false
		if listOpen == true:
			%UI.get_node("AnimationPlayer").play_backwards("open_list")
		if listOpen == false:
			%UI.get_node("AnimationPlayer").play("open_list")
		listOpen = !listOpen
	stamina_timer()
	if isFlashing == true and get_tree().get_first_node_in_group("follower"):
		get_tree().get_first_node_in_group("follower").get_node("Follower").canSee = true


var timer:float = 0.0
func stamina_timer(reset:bool = false):
	if reset == true:
		timer = 0.5
	elif timer > 0:
		timer -= get_physics_process_delta_time()



var stamina:float = 100.0:
	set(new):
		stamina = new
		%UI.get_node("Stamina").value = new
		
		
var isRegen:bool = false

func use_stamina(amount:int = 1) -> bool:
	stamina_timer(true)
	if stamina > 0.0:
		stamina -= amount * get_physics_process_delta_time()
		return true
	else:
		return false



func open_settings():
	%SettingsMenu.popup()
