extends Node
class_name Debug

var lights:bool = true

func _process(_delta):
	if Input.is_action_just_pressed("light"):
		get_parent().set_lights(!lights)
		lights = !lights
