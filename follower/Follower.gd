extends Node

@onready var navAgent:NavigationAgent3D = get_parent().get_node("NavigationAgent3D")

@export var movementSpeed:float = 4.0

@export var gravity:float = 1.0

var isFollowing:bool = false

var targetPosition:Vector3


var canSee:bool = false:
	set(new):
		if get_parent().get_node("HorrorMusic").playing == false:
			get_parent().get_node("HorrorMusic").play()
		canSee = new
		print("see state: " + str(new))
		

@onready var eye:RayCast3D = get_parent().get_node("RayCast3D")

func _process(_delta):
	if isFollowing == false:
		if Input.is_action_just_pressed("ui_accept"):
			isFollowing = !isFollowing
		return
	if canSee == true:
		navAgent.target_position = get_tree().get_first_node_in_group("player").global_position
		get_parent().get_node("Indicator").global_position = navAgent.target_position
	targetPosition = navAgent.get_next_path_position()
	if get_parent().global_position != targetPosition:
		get_parent().look_at(targetPosition)
	
	eye.look_at(get_tree().get_first_node_in_group("player").global_position + Vector3.UP)
	if eye.get_collider():
		if eye.get_collider().is_in_group("player"):
			canSee = true
		else:
			canSee = false
	else:
		canSee = false

	if Input.is_action_just_pressed("ui_accept"):
		isFollowing = !isFollowing


func _physics_process(delta):
	if isFollowing == false:
		return
	if get_parent().is_on_floor():
		get_parent().velocity.y = 0.0
	else:
		get_parent().velocity.y -= gravity * delta
	get_parent().velocity = (targetPosition - get_parent().global_position).normalized() * movementSpeed
	get_parent().move_and_slide()


func _on_death_area_body_entered(body):
	if !body.is_in_group("player"):
		return
	else:
		get_tree().current_scene.lose()
