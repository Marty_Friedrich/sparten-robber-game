extends Node3D


var gameScene:PackedScene

func _ready():
	gameScene = load("res://main.tscn")
	init_ui()
	get_node("Music").playing = true
	print("Loaded ready")
	get_tree().paused = false

var finalUnlocked:bool = false

func init_ui():
	
	var endingsAmount:int = 0
	for i in %EndingsCon.get_children():
		if SaveSystem.has(i.name):
			endingsAmount += 1
			i.visible = true
	%EndingsLabel.text = str(endingsAmount) + "/3"
	if endingsAmount >= 3:
		%HARD.visible = true
		%EndingsLabel.text = str(endingsAmount) + "/4"
	else:
		%HARD.visible = false
	if endingsAmount >= 4:
		%FinishedAll.visible = true


func play_game():
	print("plays game")
	SaveSystem.set_var("is_hard", false)
	get_tree().change_scene_to_packed(gameScene)



func settings():
	print("open settings")
	%SettingsMenu.popup()


func exit_game():
	print("exit game")


func credits():
	print("show credits")
	%CreditsMenu.popup()


func play_hard_version():
	print("play hard version")
	SaveSystem.set_var("is_hard", true)
	get_tree().change_scene_to_packed(gameScene)
