extends Control
class_name MenuBase


@export var ending:String = ""

func _ready():
	if visible == true and get_tree().current_scene.has_method("pause"):
		get_tree().current_scene.pause()

func popup():
	if ending != "":
		save()
	self.visible = true
	if get_tree().current_scene.has_method("pause"):
		get_tree().current_scene.pause()
	
	Input.mouse_mode = Input.MOUSE_MODE_VISIBLE


func save():
	SaveSystem.set_var(ending, true)
	SaveSystem.save()

var homeMenu:PackedScene

func home_menu():
	get_tree().change_scene_to_packed(homeMenu)

func retry():
	get_tree().reload_current_scene()

func close():
	self.visible = false
	if get_tree().current_scene.has_method("play"):
		get_tree().current_scene.play()
		Input.mouse_mode = Input.MOUSE_MODE_CAPTURED


