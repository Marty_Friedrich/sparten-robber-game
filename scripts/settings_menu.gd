extends MenuBase




func popup():
	if SaveSystem.has("mouse_sensetivity"):
		print("found entry, will put value in slider")
		%Mouse.value = SaveSystem.get_var("mouse_sensetivity")
	super()

func delete_save_data():
	%ConfirmDelete.visible = true

func close():
	SaveSystem.set_var("mouse_sensetivity", %Mouse.value)
	SaveSystem.save()
	if get_tree().get_first_node_in_group("player"):
		get_tree().get_first_node_in_group("player").get_node("KeryboardInput").lookSpeed = SaveSystem.get_var("mouse_sensetivity")
	super()
	if get_parent().get_node_or_null("PauseMenu"):
		get_parent().get_node("PauseMenu").close()



func delete_all_save_data():
	SaveSystem.delete_all()
	SaveSystem.save()
	get_tree().reload_current_scene()


func keep_save_data():
	%ConfirmDelete.visible = false

