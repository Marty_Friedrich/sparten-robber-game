extends Node
class_name G

var temp:Dictionary



func switch_to_packed(scene:PackedScene):
	var instance:Node = threaded_instantiate(scene)
	var thread:Thread = Thread.new()
	thread.start(get_tree().current_scene.get_parent().add_child.bind(instance), Thread.PRIORITY_NORMAL)
	


func threaded_instantiate(scene:PackedScene) -> Node:
	var foo:Thread = Thread.new()
	foo.start(instantiate.bind(scene), Thread.PRIORITY_HIGH)
	while foo.is_started():
		print("is working on instantiating")
		if foo.is_alive():
			break
	return foo.wait_to_finish()


func instantiate(scene:PackedScene) -> Node:
	print("started working on thread")
	var instance:Node = scene.instantiate()
	print("instantiated Node")
	return instance
