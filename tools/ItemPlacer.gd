@tool
extends Node
class_name ItemPlacer

func _ready():
	if Engine.is_editor_hint() == false:
		place_items()
		print("placed items")

@export var placeItems:bool = true:
	set(new):
		place_items()

@export_range(0.0, 1.0) var placeChance:float = 0.8

@export var items:Array[PackedScene]

const objects:Array[String] = ["Apple", "Lime", "Pears", "PinkCleaner", "Pomegranate", "Whine"]

func place_items():
	for i in get_tree().get_nodes_in_group("item"):
		i.queue_free()
	if !items:
		print_debug("Did not assing item list")
		return
	else:
		for i in get_tree().get_nodes_in_group("item_spot"):
			if randf() < placeChance:
				var duplicat:Area3D = items.pick_random().instantiate().duplicate()
				i.add_child(duplicat)
				duplicat.set_owner(get_tree().edited_scene_root)
	var itemAmount:Array[int] = [0, 0, 0, 0, 0, 0]
	for i in get_tree().get_nodes_in_group("item"):
		for n in objects.size():
			if objects[n] == i.name:
				itemAmount[n] += 1
	print(itemAmount)
	for i in itemAmount:
		if i == 0:
			place_items()
			return
