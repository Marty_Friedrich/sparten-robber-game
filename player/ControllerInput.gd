extends Node
class_name ControllerInput

@export var lookSpeed:float = 1.0


func _physics_process(delta):
	var playerRotation:float = get_parent().rotation.y
	var movement:Vector2 = Input.get_vector("walk_left", "walk_right", "walk_backward", "walk_forward")
	movement = movement.rotated(playerRotation)
	get_parent().velocity.x = movement.x
	get_parent().velocity.z = -movement.y
	get_parent().move_and_slide()
	
	
	var cam:Camera3D = get_parent().get_node("Camera3D")
	var lookAt:Vector2 = Input.get_vector("look_left", "look_right", "look_down", "look_up")
	cam.rotation.y -= lookAt.x * lookSpeed
	cam.rotation.x += lookAt.y * lookSpeed
	cam.rotation.x = clamp(cam.rotation.x, -1.5, 1.5)
