extends Node
class_name TouchInput

func interact():
	Input.action_press("pickup")
	await get_tree().create_timer(0.1).timeout
	Input.action_release("pickup")



func list():
	Input.action_press("open_list")
	await get_tree().create_timer(0.1).timeout
	Input.action_release("open_list")
