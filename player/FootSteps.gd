extends AudioStreamPlayer3D
class_name RandomPlayer

@export var concreteSounds:Array[AudioStream]

@export var woodSounds:Array[AudioStream]


func _process(_delta):
	if $FloorCheck.get_collider():
		if $FloorCheck.get_collider().is_in_group("Concrete"):
			floorType = "Concrete"
		if $FloorCheck.get_collider().is_in_group("Wood"):
			floorType = "Wood"

var floorType:String = "Concrete"

func _ready():
	get_tree().create_timer(10.0).timeout.connect(repeat)
	match floorType:
		"Concrete":
			stream = concreteSounds.pick_random()
		"Wood":
			stream = woodSounds.pick_random()

var isPlaying:bool = false

func stop_playing():
	isPlaying = false

func start_playing():
	if isPlaying:
		return
	isInterrupted = true
	isPlaying = true
	await get_tree().create_timer(0.5).timeout
	isInterrupted = false
	repeat()

var isInterrupted:bool = false

func repeat():
	if isInterrupted == true or isPlaying == false:
		return
	isInterrupted = false
	randomize()
	match floorType:
		"Concrete":
			stream = concreteSounds.pick_random()
			print("played sound " + str(stream.resource_name))
		"Wood":
			stream = woodSounds.pick_random()
	playing = true
	get_tree().create_timer(0.5).timeout.connect(repeat)
