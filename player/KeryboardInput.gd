extends Node
class_name KeyboardInput


@export var lookSpeed:float = 0.01

@export var walkSpeed:float = 2.0

var isRunning:bool = false

var canRun:bool = true
func _ready():
	if SaveSystem.has("mouse_sensetivity"):
		lookSpeed = SaveSystem.get_var("mouse_sensetivity")

func _physics_process(delta):
	var playerRotation:float = get_parent().rotation.y
	var movement:Vector2 = Input.get_vector("walk_left", "walk_right", "walk_backward", "walk_forward")
	movement = movement.rotated(playerRotation)
	if Input.is_action_pressed("run") and canRun == true and movement != Vector2.ZERO and get_tree().current_scene.use_stamina(10):
		get_parent().velocity.x = movement.x * delta * 100 * walkSpeed * 1.6
		get_parent().velocity.z = -movement.y * delta * 100 * walkSpeed * 1.6
	else:
		get_parent().velocity.x = movement.x * delta * 100 * walkSpeed
		get_parent().velocity.z = -movement.y * delta * 100 * walkSpeed
	if movement != Vector2.ZERO:
		get_parent().get_node("FootSteps").start_playing()
		print("tries to walk")
	else:
		get_parent().get_node("FootSteps").stop_playing()
	get_parent().move_and_slide()



@onready var cam:Camera3D = get_parent().get_node("Camera3D")

func _input(event):
	if event is InputEventMouseMotion:
		var relative:Vector2 = event.relative
		get_parent().rotation.y -= relative.x * lookSpeed
		cam.rotation.x -= relative.y * lookSpeed
		cam.rotation.x = clamp(cam.rotation.x, -1.5, 1.5)
	if event.is_action_pressed("crouch"):
		get_parent().get_node("Abilities").crouch()
	if event.is_action_released("crouch"):
		get_parent().get_node("Abilities").crouch(false)
		pass
		
	pass
