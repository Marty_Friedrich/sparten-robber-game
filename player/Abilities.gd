extends Node
class_name Abilities


@export var crouchOffset:float = 0.3

var isCrouched:bool = false

@onready var cam:Camera3D = get_parent().get_node("Camera3D"):
	set(new):
		cam = new
		print("new camera: " + cam.name)

@onready var initialCamPos:float = cam.position.y


var flashOn:bool = false

func _ready():
	if SaveSystem.get_var("is_hard") == true:
		get_parent().get_node("Camera3D/Lamp").visible = false

func _process(_delta):
	if %RayCast3D.collide_with_areas and %RayCast3D.get_collider():
		if %RayCast3D.get_collider().is_in_group("item"):
			get_tree().current_scene.get_node("Layer/UI/Hint").visible = true
			get_tree().current_scene.get_node("Layer/UI/Hint").text = "[center]" + %RayCast3D.get_collider().name
			if Input.is_action_just_pressed("pickup"):
				var typeName:String = %RayCast3D.get_collider().name
				%RayCast3D.get_collider().queue_free()
				get_tree().current_scene.collect(typeName)
		if %RayCast3D.get_collider().is_in_group("exit"):
			get_tree().current_scene.get_node("Layer/UI/Hint").visible = true
			get_tree().current_scene.get_node("Layer/UI/Hint").text = "[center]" + %RayCast3D.get_collider().name
			if Input.is_action_just_pressed("pickup"):
				get_tree().current_scene.exit_store()
	else:
		get_tree().current_scene.get_node("Layer/UI/Hint").text = ""
		get_tree().current_scene.get_node("Layer/UI/Hint").visible = false
	if Input.is_action_just_pressed("flash") and SaveSystem.get_var("is_hard") == false:
		get_tree().current_scene.hide_flash_info()
		if get_tree().current_scene.can_flash() == true:
			flashOn = !flashOn
			%Flash.visible = flashOn
			get_tree().current_scene.isFlashing = flashOn

func crouch(state:bool = true):
	if state == true and isCrouched == false:
		var TW:Tween = get_tree().create_tween()
		TW.tween_property(cam, "position:y", initialCamPos - crouchOffset, 0.1)
		isCrouched = true
	elif state == false and isCrouched == true:
		var TW:Tween = get_tree().create_tween()
		TW.tween_property(cam, "position:y", initialCamPos, 0.2)
		isCrouched = false

