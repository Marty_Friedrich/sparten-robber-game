extends Node
class_name Gravity

@export var gravity:float = 1.0

@onready var player:CharacterBody3D = get_parent()

func _physics_process(_delta):
	if player.is_on_floor():
		player.velocity.y = 0
		return
	else:
		player.velocity.y -= gravity
